var quibbler;
(function (quibbler) {
    "use strict";
    angular.module("quibbler", ["ngRoute", "ngMaterial", "ngSanitize"]);
    quibbler.getModule = function () {
        return angular.module("quibbler");
    };
})(quibbler || (quibbler = {}));
/// <reference path="../index.ts" />
var quibbler;
(function (quibbler) {
    "use strict";
    var app = quibbler.getModule();
    var HomeController = (function () {
        function HomeController($http) {
            var _this = this;
            this.$http = $http;
            this.list = [];
            this.downloadStats().then(function () {
                _this.download();
            });
        }
        HomeController.prototype.downloadStats = function () {
            var _this = this;
            return this.$http.get("data/stats.json").then(function (result) {
                _this.stats = result.data;
            }, function (error) {
                console.error(error);
            });
        };
        HomeController.prototype.download = function (type) {
            var _this = this;
            if (type === void 0) { type = "day"; }
            this.loading = true;
            this.$http.get("data/" + type + ".json").then(function (result) {
                var list = result.data;
                var scored = [];
                var sorted = [];
                if (_this.stats) {
                    angular.forEach(list, function (value, key) {
                        var divisor = 1.0;
                        var multiplier = 1.0;
                        if (_this.stats[value.subreddit]) {
                            divisor = Math.log(_this.stats[value.subreddit].subscribers + 10.0) / Math.LOG10E;
                            multiplier = Math.log(_this.stats[value.subreddit].accounts_active + 10.0) / Math.LOG10E;
                        }
                        else {
                            divisor = Math.log(10.0) / Math.LOG10E;
                            multiplier = Math.log(10.0) / Math.LOG10E;
                        }
                        value.score = (multiplier * (value.ups + (10.0 * value.comments))) / divisor;
                        scored.push(value);
                    });
                    sorted = scored.sort(function (a, b) {
                        if (a.score < b.score) {
                            return -1;
                        }
                        if (a.score > b.score) {
                            return 1;
                        }
                        return 0;
                    });
                }
                _this.list = sorted.reverse();
                _this.loading = false;
            }, function (error) {
                _this.list = [];
                _this.loading = false;
                console.error(error);
            });
        };
        return HomeController;
    }());
    HomeController.$inject = ["$http"];
    app.controller("HomeController", HomeController);
})(quibbler || (quibbler = {}));
var quibbler;
(function (quibbler) {
    "use strict";
    var app = quibbler.getModule();
    app.config([
        "$routeProvider", function ($routeProvider) {
            $routeProvider.when("/", {
                templateUrl: "views/home.html",
                controller: "HomeController",
                controllerAs: "ctrl"
            }).when("/about", {
                templateUrl: "views/about.html"
            }).when("/contact", {
                templateUrl: "views/contact.html"
            });
        }
    ]);
})(quibbler || (quibbler = {}));
