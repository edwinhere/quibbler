module quibbler {
    "use strict";
    angular.module("quibbler", ["ngRoute", "ngMaterial", "ngSanitize"]);
    export var getModule: () => ng.IModule = () => {
        return angular.module("quibbler");
    }
}