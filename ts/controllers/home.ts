/// <reference path="../index.ts" />
module quibbler {
    "use strict";
    const app = getModule();

    interface IListing {
        url: string;
        title: string;
        ups: number;
        down: number;
        permalink: string;
        created: number;
        comments: number;
        subreddit: string;
        score?: number;
    }

    interface IStat {
        subscribers: number;
        accounts_active: number;
    }

    interface IStats {
        [subreddit: string]: IStat;
    }

    class HomeController {
        private list: IListing[] = [] as IListing[];
        private stats: IStats;
        private loading: boolean;

        constructor(private $http: ng.IHttpService) {
            this.downloadStats().then(() => {
                this.download();
            });
        }

        public downloadStats(): ng.IPromise<void> {
            return this.$http.get("data/stats.json").then(
                (result: ng.IHttpPromiseCallbackArg<IStats>) => {
                    this.stats = result.data;
                },
                (error: any) => {
                    console.error(error);
                }
            );
        }

        public download(type: string = "day"): void {
            this.loading = true;
            this.$http.get("data/" + type + ".json").then(
                (result: ng.IHttpPromiseCallbackArg<IListing[]>) => {
                    var list = result.data;
                    var scored = [];
                    var sorted = []
                    if (this.stats) {
                        angular.forEach(list, (value: IListing, key: number) => {
                            var divisor = 1.0;
                            var multiplier = 1.0;
                            if(this.stats[value.subreddit]) {
                                divisor = Math.log(this.stats[value.subreddit].subscribers + 10.0) / Math.LOG10E;
                                multiplier = Math.log(this.stats[value.subreddit].accounts_active + 10.0) / Math.LOG10E;
                            } else {
                                divisor = Math.log(10.0) / Math.LOG10E;
                                multiplier = Math.log(10.0) / Math.LOG10E;
                            }
                            value.score = (multiplier * (value.ups + (10.0 * value.comments))) / divisor;
                            scored.push(value);
                        });
                        sorted = scored.sort((a: IListing, b: IListing) => {
                            if (a.score < b.score) {
                                return -1;
                            }
                            if (a.score > b.score) {
                                return 1;
                            }
                            return 0;
                        });
                    }
                    this.list = sorted.reverse();
                    this.loading = false;
                },
                (error: any) => {
                    this.list = [];
                    this.loading = false;
                    console.error(error);
                }
            );
        }

        public static $inject: string[] = ["$http"];
    }
    app.controller("HomeController", HomeController);
}