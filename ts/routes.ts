module quibbler {
    "use strict";
    var app = getModule();
    app.config([
        "$routeProvider", ($routeProvider: ng.route.IRouteProvider) => {
            $routeProvider.when("/", {
                templateUrl: "views/home.html",
                controller: "HomeController",
                controllerAs: "ctrl"
            }).when("/about", {
                templateUrl: "views/about.html"
            }).when("/contact", {
                templateUrl: "views/contact.html"
            });
        }
    ])
}