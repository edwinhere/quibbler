#!/bin/bash
(
  # Wait for lock on /var/lock/.quibbler.exclusivelock (fd 200) for 10 seconds
  flock -x -w 10 200 || exit 1

  DOMAINS=(
    4threvolutionarywar.wordpress.com
    abeldanger.net
    abovetopsecret.com
    activistpost.com
    ahtribune.com
    allnewspipeline.com
    americanlookout.com
    americanthinker.com
    americasfreedomfighters.com
    ammoland.com
    amren.com
    amtvmedia.com
    ancient-code.com
    anonews.co
    anonhq.com
    antiwar.com
    asia-pacificresearch.com
    assassinationscience.com
    baltimoregazette.com
    barenakedislam.com
    beforeitsnews.com
    bignuggetnews.com
    blackagendareport.com
    blacklistednews.com
    breitbart.com
    canadafreepress.com
    centerforsecuritypolicy.org
    christianfightback.com
    citizensunited.org
    city-journal.org
    cnsnews.com
    collective-evolution.com
    commentarymagazine.com
    conservativedailypost.com
    conservativereview.com
    consortiumnews.com
    corbettreport.com
    cosmicscientist.com
    countercurrents.org
    counterinformation.wordpress.com
    counterjihad.com
    counterpunch.org
    dailycaller.com
    dailymail.co.uk
    dailyoccupation.com
    dailysignal.com
    dailystormer.com
    darkmoon.me
    darkpolitricks.com
    davidstockmanscontracorner.com
    dcclothesline.com
    dcleaks.com
    defenddemocracy.press
    defensenews.com
    dennismichaellynch.com
    disclose.tv
    disclosuremedia.net
    dreamlandresort.com
    drudgereport.com
    educateinspirechange.org
    educate-yourself.org
    endingthefed.com
    endoftheamericandream.com
    endtime.com
    eutimes.net
    eutopia.buzz
    ewao.com
    eyeopening.info
    familysecuritymatters.org
    fbi.gov
    fellowshipoftheminds.com
    filmsforaction.org
    floridasunpost.com
    foreignpolicyjournal.com
    fourwinds10.net
    freebeacon.com
    freedomoutpost.com
    freedomworks.org
    frontpagemag.com
    gaia.com
    galacticconnection.com
    gangstergovernment.com
    gatesofvienna.net
    geopolmonitor.com
    globalresearch.ca
    godlikeproductions.com
    govtslaves.info
    greanvillepost.com
    guccifer2.wordpress.com
    haaretz.com
    hangthebankers.com
    healthnutnews.com
    heartland.org
    henrymakow.com
    heresyblog.net
    heritage.org
    heterodoxacademy.org
    hiddenamericans.com
    hotair.com
    humansarefree.com
    ihavethetruth.com
    ilovemyfreedom.org
    in5d.com
    informationclearinghouse.info
    infowars.com
    intellihub.com
    intersectionproject.eu
    intrepidreport.com
    investmentresearchdynamics.com
    investmentwatchblog.com
    jackpineradicals.com
    jamesrgrangerjr.com
    jewsnews.co.il
    jihadwatch.org
    journal-neo.org
    judicialwatch.org
    katehon.com
    katehon.org
    kingworldnews.com
    lewrockwell.com
    libertyblitzkrieg.com
    libertywritersnews.com
    lifenews.com
    lifezette.com
    makeamericagreattoday.com
    mediaite.com
    memoryholeblog.com
    mintpressnews.com
    moonofalabama.org
    nakedcapitalism.com
    nationalreview.com
    naturalblaze.com
    naturalnews.com
    newatlas.com
    newcoldwar.org
    newstarget.com
    news.usni.org
    newswithviews.com
    nowtheendbegins.com
    nutritionfacts.org
    observer.com
    off-guardian.org
    oftwominds.com
    oilgeopolitics.net
    opednews.com
    orientalreview.org
    pamelageller.com
    patriotrising.com
    paulcraigroberts.org
    peakprosperity.com
    phibetaiota.net
    pjmedia.com
    platosguns.com
    powerlineblog.com
    pravdareport.com
    pravda.ru
    prepperwebsite.com
    prisonplanet.com
    rbth.com
    readfomag.com
    readynutrition.com
    redflagnews.com
    regated.com
    rense.com
    righton.com
    rightwingnews.com
    rinf.com
    ronpaulinstitute.org
    rt.com
    rumormillnews.com
    ruptly.tv
    russia-direct.org
    russia-insider.com
    sbir.gov
    sentinelblog.com
    sentinel.ht
    sgtreport.com
    shadowproof.com
    shadowstats.com
    shiftfrequency.com
    shtfplan.com
    silentmajoritypatriots.com
    silverdoctors.com
    sott.net
    southfront.org
    speisa.com
    sputniknews.com
    stateofthenation2012.com
    stevequayle.com
    stormcloudsgathering.com
    strategic-culture.org
    superstation95.com
    survivopedia.com
    theamericanmirror.com
    theantimedia.org
    theaviationist.com
    theburningplatform.com
    thecollegefix.com
    thecommonsenseshow.com
    theconservativetreehouse.com
    thedailybell.com
    thedailysheeple.com
    theduran.com
    theearthchild.co.za
    theeconomiccollapseblog.com
    theeventchronicle.com
    thefederalistpapers.org
    thefreethoughtproject.com
    thegatewaypundit.com
    #thehill.com
    #theintercept.com
    themindunleashed.org
    thenewsdoctors.com
    the-newspapers.com
    therebel.media
    therightscoop.com
    therussophile.org
    thesaker.is
    thesleuthjournal.com
    thetruenews.info
    thetruthseeker.co.uk
    thirdworldtraveler.com
    toprightnews.com
    trueactivist.com
    truepundit.com
    trunews.com
    truthandaction.org
    truthdig.com
    truthfeed.com
    truthkings.com
    truth-out.org
    twitchy.com
    ufoholic.com
    undergroundworldnews.com
    unz.com
    usanewshome.com
    usapoliticsnow.com
    usasupreme.com
    usdcrisis.com
    usslibertyveterans.org
    vdare.com
    veteransnewsnow.com
    veteranstoday.com
    vigilantcitizen.com
    viralliberty.com
    voltairenet.org
    wakeupthesheep.com
    wakingtimes.com
    warisboring.com
    warontherocks.com
    washingtonsblog.com
    wearechange.org
    weaselzippers.us
    weeklystandard.com
    weshapelife.org
    westernjournalism.com
    whatdoesitmean.com
    whatreallyhappened.com
    wikileaks.com
    wikileaksdecrypted.com
    wikileaks.org
    wikispooks.com
    wnd.com
    worldnewspolitics.com
    worldpoliticsus.com
    www.fort-russ.com
    youngcons.com
    yournewswire.com
    zerohedge.com
  )

  MULTIS=(
    news
  )

  declare -A THRESHOLD=( [day]=1 [week]=10 [month]=100 [year]=1000 [all]=10000 )
  
  USER_AGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36"
  
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  done
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  
  for duration in day week month year all
  do
    for domain in "${DOMAINS[@]}"
    do
      torsocks curl --compressed -f -s -A "$USER_AGENT" "https://www.reddit.com/domain/${domain}/top/.json?sort=top&t=${duration}&limit=1000"
    done > "${DIR}/${duration}.all"
    for multi in "${MULTIS[@]}"
    do
      torsocks curl --compressed -f -s -A "$USER_AGENT" "https://www.reddit.com/user/beatricejensen/m/${multi}/top/.json?sort=top&t=${duration}&limit=1000"
    done >> "${DIR}/${duration}.all"
    jq -s 'reduce .[].data.children as $item ([]; . + $item)  | map(.data) | map({url: .url, title: .title, ups: .ups, downs: .downs, permalink: .permalink, created: .created_utc, comments: .num_comments, subreddit: .subreddit})  | sort_by(.ups) | reverse | unique_by(.url) | unique_by(.title) | sort_by(.ups) | reverse | map(select(.ups > '"${THRESHOLD[$duration]}"'))' "${DIR}/${duration}.all" > "${DIR}/${duration}.json"
  done
  
  cat $DIR/day.json $DIR/week.json $DIR/month.json $DIR/year.json $DIR/all.json | \
  jq --slurp --raw-output 'map(map(.subreddit)) | add | unique | .[]' > $DIR/subreddits.all
  while IFS= read -r line
  do
     torsocks curl --compressed -f -s -A "$USER_AGENT" "https://www.reddit.com/r/$line/about.json"
  done < "${DIR}/subreddits.all" > "${DIR}/subreddits.json"
  cat $DIR/subreddits.json | jq --slurp 'map({"\(.data.display_name)":{subscribers: .data.subscribers, accounts_active: .data.accounts_active}}) | add' > $DIR/stats.json
) 200>/var/lock/.quibbler.exclusivelock
